none:
	$(error must provide a target (e.g. windows, windows-from-wsl, linux-from-wsl))
windows:
	go build -ldflags "-buildid= -w -s -H windowsgui" -trimpath .
windows-from-wsl:
	CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc-win32 GOOS=windows GOARCH=amd64 go build -ldflags "-buildid= -w -s -H windowsgui" -trimpath .
linux-from-wsl:
	go build -ldflags "-buildid= -w -s" -trimpath .
