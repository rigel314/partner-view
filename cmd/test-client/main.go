package main

import (
	"context"
	"crypto/x509"
	"flag"
	"log"
	"net"

	"gitlab.com/rigel314/partner-view/config"
	"gitlab.com/rigel314/partner-view/reg"
)

var configpath = flag.String("cfg", "", "path to config directory, default: executable dir")

func main() {
	flag.Parse()
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	s, err := config.GetSecrets(*configpath)
	if err != nil {
		log.Fatal("failed to read/initialize secrets file: ", err)
	}

	if len(s.Cert.Certificate) < 1 {
		log.Fatal("no secret certificate loaded: ", err)
	}
	s.Cert.Leaf, err = x509.ParseCertificate(s.Cert.Certificate[0])
	if err != nil {
		log.Fatal("failed to parse secret cert: ", err)
	}
	// self := s.Cert.Leaf
	// cp := x509.NewCertPool()
	// cp.AddCert(self)

	// c, err := tls.Dial("tcp", "127.0.0.1:49365", nil)
	// if err != nil {
	// 	log.Fatal("failed to dial: ", err)
	// }
	// time.Sleep(2 * time.Second)
	// // _, err = c.Write([]byte("hello\n"))
	// // if err != nil {
	// // 	log.Fatal("failed to write: ", err)
	// // }
	// time.Sleep(time.Second)
	// c.Close()

	c, err := net.DialTCP("tcp", nil, &net.TCPAddr{IP: net.IPv4(127, 0, 0, 1), Port: 49366})
	if err != nil {
		log.Fatal("failed to dial, ", err)
	}
	cert, err := reg.Register(context.Background(), c, s.Cert.Leaf, s.Cert.PrivateKey)
	if err != nil {
		log.Fatal("failed to register, ", err)
	}
	log.Println(cert)
	c.Close()
}
