package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"reflect"
)

// Config holds the structure of the main config file
type Config struct {
	PartnerName               string `wset:"title=Partner Name;description=Your partner name as shown to others"`
	ExitOnMainClose           bool   `wset:"title=Exit when main window closed;description=Setting this will cause partner-view to exit completely when this window is closed"`
	TrayIcon                  bool   `wset:"title=Create system tray icon;description=Attempts to shows an icon in the notification area.  This does not work on all platforms."`
	NotifyOnNewPartnerRequest bool   `wset:"title=Show notificaton window on new partner request;description=Prompt the user when someone attempts to become a new partner, instead of having to confirm/deny in the Partners section"`
	ShowMainWindowOnStart     bool   `wset:"title=Show this window on launch;description=When unset, partner-view will run in the background on launch.  Launching it again or interacting with the system tray icon will show this window again."`
	DoNotDisturb              bool   `wset:"title=Do not disturb;description=Prevents parters from flinging views onto your screen, also prevents accidental sharing"`
	ViewSizeX                 int    `wset:"title=Horizontal View Size;description=Default size of view windows"`
	ViewSizeY                 int    `wset:"title=Vertical View Size;description=Default size of view windows"`
	Debug                     configDebug
}

type configDebug struct {
	UniqueLogNames bool `wset:"title=Unique log names;description=Append a timestamp to logfile to get a new file every launch"`
}

var defConfig = Config{
	PartnerName:               "My Partner Name",
	ExitOnMainClose:           false,
	TrayIcon:                  true,
	NotifyOnNewPartnerRequest: true,
	ShowMainWindowOnStart:     false,
	DoNotDisturb:              false,
	ViewSizeX:                 1280,
	ViewSizeY:                 1024,
	Debug: configDebug{
		UniqueLogNames: false,
	},
}

// GetConfig reads the config file, initializing first if necessary, and returns a Config
func GetConfig(dir string) (*Config, error) {
	var c Config
	err := getOrInitFile(dir, "partner-view.json", &c, initConfFromDefault(defConfig))
	if err != nil {
		return nil, err
	}
	return &c, nil
}

func initConfFromDefault(def interface{}) func(*os.File) error {
	return func(f *os.File) error {
		je := json.NewEncoder(f)
		je.SetIndent("", "\t")
		je.Encode(def)

		return nil
	}
}

func getOrInitFile(dir, fname string, outval interface{}, initfn func(*os.File) error) error {
	var execdir string
	if dir == "" {
		execpath, err := os.Executable()
		if err != nil {
			return err
		}
		execdir = filepath.Dir(execpath)
	} else {
		execdir = dir
	}
	conffile := filepath.Join(execdir, fname)
	log.Println(conffile)
	conff, err := os.OpenFile(conffile, os.O_RDWR, 0600)
	if err != nil && os.IsNotExist(err) {
		conff, err = os.OpenFile(conffile, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	}
	defer conff.Close()

	tries := 0
	for tries < 2 {
		jd := json.NewDecoder(conff)
		err = jd.Decode(&outval)
		if tries == 0 && (err != nil || reflect.DeepEqual(outval, reflect.New(reflect.TypeOf(outval).Elem()))) {
			_, err := conff.Seek(0, os.SEEK_SET)
			if err != nil {
				return err
			}
			err = initfn(conff)
			if err != nil {
				return err
			}
			_, err = conff.Seek(0, os.SEEK_SET)
			if err != nil {
				return err
			}
		} else if err != nil {
			return err
		} else {
			tries = 3
		}
		tries++
	}
	if tries == 2 {
		return fmt.Errorf("Failed to read/init file")
	}

	return nil
}
