package config

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"encoding/pem"
	"log"
	"math/big"
	"os"
	"time"
)

// TODO: use https://github.com/awnumar/memguard

// Secrets contains the self secrets (key and cert)
type Secrets struct {
	Cert tls.Certificate
}

type secretsFile struct {
	CertPEMBlock *string
	KeyPEMBlock  *string
}

// GetSecrets reads the secrets file, initializing first if necessary, and returns a Secrets
func GetSecrets(dir string) (*Secrets, error) {
	var sf secretsFile
	err := getOrInitFile(dir, ".partner-view.secrets.json", &sf, initSecrets)
	if err != nil {
		return nil, err
	}
	c, err := tls.X509KeyPair([]byte(*sf.CertPEMBlock), []byte(*sf.KeyPEMBlock))
	return &Secrets{
		Cert: c,
	}, err
}

func initSecrets(f *os.File) error {
	log.Println("reinitializing local secrets, all parters will have to re-register")

	k, c, err := GenCert(true)
	if err != nil {
		return err
	}
	je := json.NewEncoder(f)
	sc := string(c)
	sk := string(k)
	s := secretsFile{
		CertPEMBlock: &sc,
		KeyPEMBlock:  &sk,
	}
	je.SetIndent("", "\t")
	je.Encode(s)

	return nil
}

// GenCert creates a new certificate pair, with the correct options for partner-view
func GenCert(ca bool) (key []byte, cert []byte, err error) {
	priv, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	notBefore := time.Now().Add(time.Hour * -24)
	notAfter := notBefore.Add(time.Hour * 24 * 365 * 200)
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, nil, err
	}
	c := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"partner-view"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageDataEncipherment | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
		BasicConstraintsValid: true,
		IsCA:                  ca,
		DNSNames:              []string{"*"},
	}
	cBytes, err := x509.CreateCertificate(rand.Reader, &c, &c, &priv.PublicKey, priv)
	if err != nil {
		return nil, nil, err
	}
	cert = pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: cBytes})

	privBytes, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		return nil, nil, err
	}
	key = pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: privBytes})

	return key, cert, nil
}
