module gitlab.com/rigel314/partner-view

go 1.13

require (
	fyne.io/fyne v1.2.3
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/getlantern/ops v0.0.0-20200403153110-8476b16edcd6 // indirect
	github.com/getlantern/systray v0.0.0-20200324212034-d3ab4fd25d99
	github.com/go-gl/glfw v0.0.0-20200222043503-6f7a984d4dc4 // indirect
	github.com/lxn/walk v0.0.0-20191128110447-55ccb3a9f5c1 // indirect
	github.com/lxn/win v0.0.0-20191128105842-2da648fda5b4 // indirect
	github.com/srwiley/oksvg v0.0.0-20200311192757-870daf9aa564 // indirect
	github.com/srwiley/rasterx v0.0.0-20200120212402-85cb7272f5e9 // indirect
	gitlab.com/rigel314/go-event-frequency v1.0.0
	gitlab.com/rigel314/window-settings-fyne v0.0.0-20200404043537-69d7ae7b6255
	golang.org/x/exp v0.0.0-20200331195152-e8c3332aa8e5 // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
	golang.org/x/mobile v0.0.0-20200329125638-4c31acba0007 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200331124033-c3d80250170d
	golang.org/x/text v0.3.2 // indirect
)

// replace gitlab.com/rigel314/go-event-frequency => ../go-event-frequency

// replace gitlab.com/rigel314/window-settings-fyne => ../window-settings-fyne
