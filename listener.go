package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"log"
	"net"
	"time"

	"gitlab.com/rigel314/partner-view/config"
	"gitlab.com/rigel314/partner-view/reg"
)

func partnerListener() {
	s, err := config.GetSecrets(*configpath)
	if err != nil {
		log.Fatal("failed to read/initialize secrets file: ", err)
	}

	if len(s.Cert.Certificate) < 1 {
		log.Fatal("no secret certificate loaded: ", err)
	}
	s.Cert.Leaf, err = x509.ParseCertificate(s.Cert.Certificate[0])
	if err != nil {
		log.Fatal("failed to parse secret cert: ", err)
	}
	self := s.Cert.Leaf
	selfcp := x509.NewCertPool()
	selfcp.AddCert(self)

	partnercp := x509.NewCertPool()
	partnercp.AddCert(self)

	go registerListener(self, s)

	l, err := tls.Listen("tcp", "0.0.0.0:49365", &tls.Config{
		Certificates: []tls.Certificate{s.Cert},
		RootCAs:      partnercp,
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    selfcp,
	})
	if err != nil {
		log.Fatal("failed to create main partner server:", err)
	}
	defer l.Close()
	log.Println("tls server running")
	for {
		c, err := l.Accept()
		if err != nil {
			log.Fatal("tls Accept error: ", err)
		}
		log.Println("connection from: ", c.RemoteAddr())
		time.Sleep(3 * time.Second)
		_, err = c.Write([]byte("hello\n"))
		if err != nil {
			log.Fatal("failed to write: ", err)
		}
		time.Sleep(time.Second)
		c.Close()
	}
}

func registerListener(selfcert *x509.Certificate, s *config.Secrets) {
	l, err := net.ListenTCP("tcp", &net.TCPAddr{IP: net.IPv4(0, 0, 0, 0), Port: 49366})
	if err != nil {
		log.Fatal("failed to create registration partner server")
	}
	defer l.Close()
	log.Println("registration server running")
	for {
		client, err := l.Accept()
		if err != nil {
			log.Println("tcp Accept error: ", err)
			continue
		}
		go func() {
			defer client.Close()
			reg.Register(context.Background(), client, s.Cert.Leaf, s.Cert.PrivateKey)
		}()
	}
}
