package main

import (
	"flag"
	"image"
	_ "image/png"
	"log"
	"sync"
	"sync/atomic"
	"time"

	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	"github.com/getlantern/systray"
	"gitlab.com/rigel314/partner-view/config"
	wset "gitlab.com/rigel314/window-settings-fyne"
)

//go:generate go run github.com/akavel/rsrc -manifest data/app.manifest -arch=386   -o pv_windows_386.syso
//go:generate go run github.com/akavel/rsrc -manifest data/app.manifest -arch=amd64 -o pv_windows_amd64.syso

// TODO: add screenshots https://godoc.org/github.com/kbinani/screenshot

var (
	configpath = flag.String("cfg", "", "path to config directory, default: executable dir")
)

func init() {
	flag.Parse()
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

var cfgval atomic.Value

func main() {
	imageCh := make(chan *image.RGBA)
	quitCh := make(chan struct{})

	// TODO: check single instance running https://github.com/marcsauter/single

	cfg, err := config.GetConfig(*configpath)
	if err != nil {
		log.Fatal("Couldn't load config, ", err)
	}

	cfgval.Store(cfg)

	go tray(quitCh, imageCh)
	go partnerListener()

	a := app.NewWithID(appID)

	// Hidden master window allows closing other windows while keeping app running
	hw := a.NewWindow("Partner View Hidden")
	hw.SetContent(widget.NewButton("Quit", func() { a.Quit() }))
	hw.SetMaster()

	// Main window
	mw := a.NewWindow("Partner View Config")
	tabs := widget.NewTabContainer(
		widget.NewTabItemWithIcon("Partners", theme.ContentCopyIcon(), widget.NewVBox(
			widget.NewLabelWithStyle("TODO: partner list", fyne.TextAlignCenter, fyne.TextStyle{Bold: true}),
		)),
		widget.NewTabItemWithIcon("Settings", theme.SettingsIcon(), wset.ConfigScreen(cfgval)),
	)
	tabs.SetTabLocation(widget.TabLocationLeading)
	mw.SetContent(tabs)
	mw.Resize(fyne.Size{
		Width:  int(640 / mw.Canvas().Scale()),
		Height: int(480 / mw.Canvas().Scale()),
	})
	mw.CenterOnScreen()

	go winLoop(imageCh, quitCh, a)

	mw.Show()
	a.Run()

	systray.Quit()
	time.Sleep(time.Second)
}

func winLoop(imageCh chan *image.RGBA, quitCh chan struct{}, a fyne.App) {
windowLoop:
	for {
		var rgba *image.RGBA
		select {
		case v, ok := <-imageCh:
			if !ok {
				break windowLoop
			}
			rgba = v
		case <-quitCh:
			break windowLoop
		}

		// View window for showing images
		vw := a.NewWindow("Partner View - <partner name>")
		vwClosed := make(chan struct{})
		var once sync.Once
		vw.SetOnClosed(func() {
			once.Do(func() {
				// Once to ensure I don't call vw.Close() twice and panic
				close(vwClosed)
			})
		})
		img := canvas.NewImageFromImage(rgba)
		img.FillMode = canvas.ImageFillContain
		img.SetMinSize(fyne.Size{
			Width:  100,
			Height: 100,
		})
		vw.SetContent(img)
		vw.Resize(fyne.Size{
			Width:  fyne.Min(rgba.Bounds().Dx(), int(1280/vw.Canvas().Scale())),
			Height: fyne.Min(rgba.Bounds().Dy(), int(1024/vw.Canvas().Scale())),
		})
		vw.CenterOnScreen()
		vw.Show()

	renderLoop:
		for {
			select {
			case <-vwClosed:
				break renderLoop
			case <-quitCh:
				break renderLoop
			case v := <-imageCh:
				img.Image = v
				img.Refresh()
			}
		}
	}

	a.Quit()
}
