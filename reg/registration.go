/*
Package reg holds the types for the process of initial partner registration

Initial registration process

	1. Client connects to server on reg port(tcp,49366)
	2. Client->Server, send Request
	3. Server->Client, send Request
	4. Client->Server, user verifies Server name, send Response
	5. Server->Client, user verifies Client name, send Response

In the above example, steps 2 and 3 may happen simultaniously.  Steps 4 and 5, must happen in
order.  Because the client is the one initiating the connection, the server shouldn't trust it
until the client has sent something valid.  This isn't garunteed to make anything secure, but I
think it makes sense that the client must sign the server's cert before the server will sign the
client's cert.
*/
package reg

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/gob"
	"fmt"
	"log"
	"net"
	"reflect"
)

// A Request is sent by each side after connection to registration port.
type Request struct {
	Name            string            // Descriptive name, shown to user on other end
	CertSignRequest *x509.Certificate // Cert to be signed by other end
}

// A Response is sent after local user allows it.  For the server side, a Response will only be sent after recieving the client Response.
type Response struct {
	CertSignResponse *x509.Certificate // Request cert signed by this side
}

func init() {
	gob.Register(&ecdsa.PublicKey{})
	gob.Register(&elliptic.CurveParams{})
}

// Register performs the initial registration process with a client
func Register(ctx context.Context, c net.Conn, self *x509.Certificate, priv crypto.PrivateKey) (*x509.Certificate, error) {
	cdec := gob.NewDecoder(c)
	senc := gob.NewEncoder(c)

	var creq Request
	var cresp Response

	// send request
	err := senc.Encode(Request{
		Name:            "server", // TODO: replace with configured name
		CertSignRequest: self,
	})
	if err != nil {
		log.Println("encode error: ", err)
		return nil, err
	}

	// read request from other end
	err = cdec.Decode(&creq)
	if err != nil {
		log.Println("decode error: ", err)
		return nil, err
	}

	log.Println("got request from: ", creq.Name, ", accepting...")

	// build response
	cpk, ok := creq.CertSignRequest.PublicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Println("public key not ecdsa")
		return nil, fmt.Errorf("public key not ecdsa")
	}
	if !reflect.DeepEqual(cpk.Curve, elliptic.P521()) {
		log.Println("public key not ecdsa521")
		return nil, fmt.Errorf("public key not ecdsa521")
	}
	cpk.Curve = elliptic.P521()
	cBytes, err := x509.CreateCertificate(rand.Reader, creq.CertSignRequest, self, creq.CertSignRequest.PublicKey, priv)
	if err != nil {
		log.Println("failed to sign cert: ", err)
		return nil, err
	}
	newcert, err := x509.ParseCertificate(cBytes)
	if err != nil {
		log.Println("failed to parse new cert: ", err)
		return nil, err
	}

	// send response
	err = senc.Encode(Response{
		CertSignResponse: newcert,
	})
	if err != nil {
		log.Println("encode error: ", err)
		return nil, err
	}

	// read response from other side
	err = cdec.Decode(&cresp)
	if err != nil {
		log.Println("decode error: ", err)
		return nil, err
	}

	return cresp.CertSignResponse, nil
}
