package tid

import "syscall"

func GetTid() int {
	return int(syscall.GetTid())
}
