//+build !windows,!linux

package tid

import "log"

func GetTid() int {
	log.Println("tid.GetTid not supported, returning -1 anyway")
	return -1
}
