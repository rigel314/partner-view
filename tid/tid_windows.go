package tid

import "golang.org/x/sys/windows"

func GetTid() int {
	return int(windows.GetCurrentThreadId())
}
