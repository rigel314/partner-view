package main

import (
	"image"
	"image/draw"
	"log"
	"os"
	"time"

	"github.com/getlantern/systray"
	"github.com/getlantern/systray/example/icon"
	freq "gitlab.com/rigel314/go-event-frequency"
)

func tray(quitCh chan struct{}, imageCh chan *image.RGBA) {
	// used to start/stop the demo
	demoCh := make(chan bool)

	systray.Run(func() {
		// setup tray information
		systray.SetIcon(icon.Data)
		systray.SetTitle("Partner View")
		systray.SetTooltip("Partner View")
		mDemo := systray.AddMenuItem("Demo", "Demo")
		mStopDemo := systray.AddMenuItem("Stop Demo", "Stop Demo")
		mStopDemo.Hide()
		mQuit := systray.AddMenuItem("Quit", "Quit the whole app")

		log.Println("systray running")
		go func() {
			_, ok := <-mQuit.ClickedCh
			if ok {
				log.Println("quitting")
				close(quitCh)
			}
		}()
		go func() {
			for range mStopDemo.ClickedCh {
				mStopDemo.Hide()
				mDemo.Show()
				demoCh <- false
			}
		}()
		go func() {
			for range mDemo.ClickedCh {
				mDemo.Hide()
				mStopDemo.Show()
				demoCh <- true
			}
		}()
		go demo(demoCh, imageCh)
	},
		nil)

}

func demo(demoCh chan bool, imageCh chan *image.RGBA) {
	var imgs []image.Image
	for _, file := range []string{"front.png", "right.png", "back.png", "left.png"} {
		imgFile, err := os.Open(file)
		if err != nil {
			panic(err)
		}
		img, _, err := image.Decode(imgFile)
		if err != nil {
			panic(err)
		}
		imgs = append(imgs, img)
		imgFile.Close()
	}
	for v := range demoCh {
		if !v {
			continue
		}
		log.Println("demo started")
		angle := 0
		ec := freq.Init(100)
	demoLoop:
		for {
			tch := time.After(100 * time.Millisecond)

			var idxL, idxR int
			if angle >= 0 && angle < 90 {
				idxL, idxR = 0, 1
			} else if angle >= 90 && angle < 180 {
				idxL, idxR = 1, 2
			} else if angle >= 180 && angle < 270 {
				idxL, idxR = 2, 3
			} else if angle >= 270 && angle < 360 {
				idxL, idxR = 3, 0
			} else {
				idxL, idxR = 0, 1
			}

			rgba := image.NewRGBA(imgs[0].Bounds())
			if rgba.Stride != rgba.Rect.Size().X*4 {
				panic("unsupported stride")
			}
			rL := demoRectFromAngle(rgba.Bounds(), angle)
			rR := demoOtherRect(rgba.Bounds(), rL)
			draw.Draw(rgba, rL, imgs[idxL], image.Point{rgba.Bounds().Max.X - rL.Max.X, 0}, draw.Src)
			draw.Draw(rgba, rR, imgs[idxR], image.Point{0, 0}, draw.Src)

			log.Println("demo send image")
			imageCh <- rgba // assume downstream can handle this fast enough

			log.Println(ec.Event())

			angle += 2
			angle %= 360

			for {
				select {
				case v := <-demoCh:
					if !v {
						log.Println("demo stopped")
						break demoLoop
					}
					// If demo was started twice, which shouldn't be possible, just wait another time
				case <-tch:
					// If demo was started a bunch of times, which shouldn't be possible, the time.After() channels will "leak" for a second each, then be garbage collected
					continue demoLoop
				}
			}
		}
	}
}

func demoRectFromAngle(r image.Rectangle, angle int) image.Rectangle {
	return image.Rectangle{
		Min: image.Point{
			X: r.Min.X,
			Y: r.Min.Y,
		},
		Max: image.Point{
			X: int(float32(r.Max.Y) * (1 - ((float32(angle % 90)) / 90))),
			Y: r.Max.Y,
		},
	}
}

func demoOtherRect(img image.Rectangle, first image.Rectangle) image.Rectangle {
	return image.Rectangle{
		Min: image.Point{
			X: first.Max.X,
			Y: img.Min.Y,
		},
		Max: image.Point{
			X: img.Max.X,
			Y: img.Max.Y,
		},
	}
}
